<?php

class MarsRover{
    public int $x;
    public int $y;

    public function __construct()
    {
        $this->x = 0;
        $this->y = 0;
    }
}

class Grid{
    public array $grid;

    public function __construct(){
        $this->grid = $this->generateGrid();
    }
    
    public function generateGrid():array
    {
        $array = [];
            for($x = 0;$x < 10;$x++){
                $arr = [];
                for($y = 0;$y < 10;$y++){
                    $arr[$y] = 0;
                }
                $array[$x] = $arr;
            }
        return $array;
    }
}