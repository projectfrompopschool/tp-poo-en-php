<?php

    class Cercle
    {
        public int $rayon;


        public function __construct(int $value)
        {
            $this->rayon = $value;
        }

        public function perimeter():int
        {
            return $this->rayon * 2;
        }

        public function Area():float
        {
            return pi() * $this->rayon * $this->rayon;
        }
    }

$rayon = new Cercle(5);
$rayonTwo = new Cercle(7);

// echo "Value perimeter: " . $rayon->perimeter();
// echo "Value perimeter: " . $carreTwo->perimeter();

echo "Value diamètre: " . round($rayon->perimeter());
echo "Value de l'air: " . round($rayonTwo->Area(),2);
?>
