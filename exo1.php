<?php

class Carre
{
    public int $cote;
    // private string $name;
    // protected float $num;

    public function __construct(int $value)
    {
        $this->cote = $value;
    }

    public function perimeter():int
    {
        return $this->cote * 4;
    }

    public function Area():int
    {
        return $this->cote * $this->cote;
    }
}

$carre = new Carre(5);
$carreTwo = new Carre(10);

echo "Value perimeter: " . $carre->perimeter();
echo "Value perimeter: " . $carreTwo->perimeter();

echo "Value Area: " . $carre->Area();
echo "Value Area: " . $carreTwo->Area();
?>


<!-- var_dump($carre); -->


<!-- echo "Hello world";
$hello = "a";


 -->








